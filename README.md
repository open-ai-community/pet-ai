# INSTALLATION

- pip install nltk
- python
- ">>> import nltk"
- ">>> nltk.download()"

- punkt
- wordnet


# TUTORIALS
Simple Chatbot:
<https://medium.com/analytics-vidhya/building-a-simple-chatbot-in-python-using-nltk-7c8c8215ac6e>

Generative Chatbot:
<https://medium.com/botsupply/generative-model-chatbots-e422ab08461e>
